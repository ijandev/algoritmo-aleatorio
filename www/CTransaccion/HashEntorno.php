<?php

include_once("/var/www/html/CDatos/hashActual.php");

class HashEntorno {    
    private $hashOriginal;

    public function __construct() {
        $this->hashOriginal = file_get_contents('/var/www/html/CDatos/hash');
    }


    public function generarHash($directory)
    {
        if (! is_dir($directory))
        {
            return false;
        }
     
        $files = array();
        $dir = dir($directory);
        while (false !== ($file = $dir->read()))
        {
            if ($file != '.' and $file != '..')
            {                    
                
                    if (is_dir($directory . '/' . $file) && $file != 'Pruebas' && $file != 'CVista')
                {   
                    $files[] = $this->generarHash($directory . '/' . $file);
                }
                else
                {
                    if ($file != "hash" && $file != "hashActual.php") {
                        $files[] = md5_file($directory . '/' . $file);
                    }
                    
                }


            }
        }
     
        $dir->close();
        return md5(implode('', $files));
    }





    public function verificarHash($arrayDirectorios)
    {
        $hashActual = $this->generarHash($arrayDirectorios[0]);
       imprimir($hashActual, $this->hashOriginal);
        if ($hashActual!=$this->hashOriginal){
			throw new Exception('El entorno ha sido alterado');
        }


     }


}

?>
