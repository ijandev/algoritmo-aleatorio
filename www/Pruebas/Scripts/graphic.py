from numpy.core.records import array
import numpy as np
import pandas as pd
import time
import matplotlib.pyplot as plt
from pandas.core.frame import DataFrame

from os import listdir
from os.path import isfile, join

if __name__ == "__main__":
    n = 100
    # Se toman las primeras 10 columnas del dataframe
    ran = list(range(1,11))
    # ruta del archivo a analizar
    path = f"csv/{n}/Df-100.csv"
    df = pd.read_csv(path,usecols=ran,na_filter=False)
    pd.options.display.float_format = "{:.2f}".format
    # Resumen estadistico
    print(df.describe())
    # Grafico de cajas
    df.boxplot()
    plt.savefig(f'{path}/boxplot-{n}.png')
    # Histograma: bins tiene que ser igual al n
    ax = df.plot.hist(bins=100,grid=False,figsize=(20, 10),fontsize=15,rot=45,edgecolor='blue')
    plt.ylabel('frequencia')
    plt.xlabel('valores')
    plt.title('Histograma')
    plt.savefig(f'{path}/Histograma-{n}.png')
