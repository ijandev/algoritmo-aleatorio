<?php

class Usuario{

	private $nombre;
	private $pass;
	private $tabla ;
	private $campos;
	private $mensajeoperacion;

	public function __construct($param){
		$this->tabla = "usuario";
		$this->campos = "nombre,pass";
		$this->nombre = (isset($param['nombre']))?$param['nombre'] : null  ;	
		
	}

	public function getTabla(){
		return $this->tabla;
	}
	
	public function getCampos(){
		return $this->campos;
	}


	public function setIdUsuario($param){
		$this->idusuario = $param;
	}

	public function getIdUsuario(){
		return $this->idusuario;
	}

	public function setNombre($param){
		$this->nombre = $param;
	}

	public function getNombre(){
		return $this->nombre;
	}

	public function setPass($param){
		$this->pass = $param;
	}
   

	public static function listar($condicion=""){
	    $coleccion = null;
		$base=new BaseDatos();
		$consulta ="Select * from  usuario";
		if ($condicion!=""){
		    $consulta=$consulta.' where '.$condicion;
		}
		//echo $consulta;
		if($base->Iniciar()){
			if($base->Ejecutar($consulta)){				
				$coleccion= array();
				while($row2=$base->Registro()){
					$obj = new Usuario($row2);
					$obj->setIdUsuario($row2['idusuario']);
					array_push($coleccion,$obj);
	
				}
				
			
		 	}	
		 }	
		 return $coleccion;
	}	



}
?>
