<?php

class Respuesta{

	private $secuencia_aleatoria;
	private $objSolicitud;
	private $idrespuesta;
	private $fecha;
	private $tabla ;
	private $campos;
	private $mensajeoperacion;
	

	public function __construct($param){
		$this->tabla = "respuesta";
		$this->campos = "idsolicitud,secuencia_aleatoria";
		$this->secuencia_aleatoria = implode(";", $param['sec_al']); 
	}

	public function getTabla(){
		return $this->tabla;
	}
	
	public function getCampos(){
		return $this->campos;
	}


	public function setObjSolicitud($param){
		$this->objSolicitud=$param;
	}

	public function getObjSolicitud(){
		
		return $this->objSolicitud;
	}

  	
	public function setIdRespuesta($param){
		$this->idrespuesta=$param;
	}

	public function getIdRespuesta(){
		return $this->idrespuesta;
	}


	public function setSecuencia_aleatoria($param){
		$this->secuencia_aleatoria=$param;
	}

	public function getSecuencia_aleatoria(){
		return $this->secuencia_aleatoria;
	}






	public function setFecha($param){
		$this->fecha=$param;
	}

	public function getFecha(){
		return $this->fecha ;
	}


	public function setmensajeoperacion($param){

		$this->mensajeoperacion =$param;
	}
	
	public function getmensajeoperacion(){
		return $this->mensajeoperacion ;
	}

	/**
	 * Recupera los datos dada su clave
	 * @param $clave
	 * @return true en caso de encontrar los datos, false en caso contrario 
	 */		
    public function Buscar($clave){
		$base=new BaseDatos();
		$consultaPersona="Select * from ".$this->tabla ."  where idrespuesta=".$clave['idrespuesta'];
		$resp= false;
		if($base->Iniciar()){
			if($base->Ejecutar($consultaPersona)){
				if($row2=$base->Registro()){
					$this->setIdRespuesta($clave['idrespuesta']);					
				    $this->setFecha($row2['fecha']);
					
					$objSol = new Solicitud($row2);
					$objSol->Buscar();
					$resp= true;
				}				
			
		 	}	else {
		 			$this->setmensajeoperacion($base->getError());
		 		
			}
		 }	else {
		 		$this->setmensajeoperacion($base->getError());
		 	
		 }		
		 return $resp;
	}	
    

	public static function listar($condicion=""){
	    $coleccion = null;
		$base=new BaseDatos();
		$consulta ="Select * from  ".$this->tabla;
		if ($condicion!=""){
		    $consulta=$consulta.' where '.$condicion;
		}
		//echo $consulta;
		if($base->Iniciar()){
			if($base->Ejecutar($consulta)){				
				$coleccion= array();
				while($row2=$base->Registro()){

					$obj = new Solicitud();
					$obj->cargar($row2);
					array_push($coleccion,$obj);
	
				}
				
			
		 	}	
		 }	
		 return $coleccion;
	}	


	
	public function insertar(){
		try {
			
			$base=new BaseDatos();
		$resp= false;

		$consultaInsertar="INSERT INTO ".$this->getTabla()."(".$this->getCampos().") 
				VALUES (".$this->getObjSolicitud()->getIdsolicitud().",'".$this->getSecuencia_aleatoria()."')";
		//echo $consultaInsertar;
		if($base->Iniciar()){

			if($base->Ejecutar($consultaInsertar)){
			    $resp=  true;
			}else{
					$this->setmensajeoperacion($base->getError());
			}
		} else {
				$this->setmensajeoperacion($base->getError());
			
		}


		} catch (Exception $e) {
			throw new Exception($e);
		}
		

		return $resp;
	}
	

	public function modificar(){
	    $resp =false; 
	    $base=new BaseDatos();
		$consultaModifica="UPDATE ". $this->getTabla()." SET secuencia_aleatoria='".$this->getSecuencia_aleatoria().
                           "' WHERE idrespuesta =". $this->getIdRespuesta();
		if($base->Iniciar()){
			if($base->Ejecutar($consultaModifica)){
			    $resp=  true;
			}else{
				$this->setmensajeoperacion($base->getError());
				
			}
		}else{
				$this->setmensajeoperacion($base->getError());
			
		}
		return $resp;
	}
	
	public function eliminar(){
		$base=new BaseDatos();
		$resp=false;
		if($base->Iniciar()){
				$consultaBorra="DELETE FROM $this->getTabla() WHERE idrespuesta=".$this->getIdRespuesta();
				if($base->Ejecutar($consultaBorra)){
				    $resp=  true;
				}else{
						$this->setmensajeoperacion($base->getError());
					
				}
		}else{
				$this->setmensajeoperacion($base->getError());
			
		}
		return $resp; 
	}

	
}
?>
