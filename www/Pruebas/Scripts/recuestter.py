import requests
import time
import random as rm


def make_request(url, auth, payload={'n': '15'}, files=[], headers={}):
    response = requests.post(url, auth= auth,
                  data=payload, headers=headers)
    return response

if __name__ == "__main__":
    url = "http://localhost/index.php"
    auth = ("usuario", "contraseña")
    n = 100
    sec = 1000000
    payload = {'n': n}
    iterations = range(1,sec)
    for i in iterations:
        res = make_request(url, auth, payload)
