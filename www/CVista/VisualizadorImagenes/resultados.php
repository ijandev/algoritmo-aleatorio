<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
.navbar {
      margin-bottom: 0;
      border-radius: 0;
}
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {height: 450px}
    
    /* Set gray background color and 100% height */
.sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
}
    
    /* Set black background color, white text and some padding */
html, body {
    margin: 0;
    height: 100%;
}
    
    /* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
  .sidenav {
        height: 100%;
        padding: 0px;
  }
  .row.content {height:100%;} 
}
  </style>
  <title></title>
</head>
<body>


<?php include 'Estructura/header.php';?>





  <div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      
    </div>
    <div id="imglist" class="col-sm-8 text-left"> 
      <h1>Pruebas</h1>
      <p>A la hora de desarrollar un generador de secuencias pseudialeatorio, se busca que cada número tenga la misma probabilidad de aparición en cada posición de la secuencia. Es decir, la ocurrencia de cada número se distribuye uniformemente.</p>
      <hr>
      <div id="demo"></div>

      <script>
        for (var i = 0; i < 3; i++) {
          var h3 = document.createElement("h3");
          h3.innerHTML = "Gráfico "+(i+1);
          document.getElementById("demo").appendChild(h3);

          if (i==0) {
            var p = document.createElement("p");
          p.innerHTML = "En el Gráfico se observa un resumen estadístico del dataframe que se utilizo para analizar el algoritmo, donde indica la media, el desvió estándar, el valor mínimo y máximo de cada secuencia, como así también los cuartiles. Estos valores fueron calculados por cada columna del set de datos, eso indica que cada columna representa un puesto en la secuencia resultante.";
          document.getElementById("demo").appendChild(p);

          var img = document.createElement("img");
          img.src = "../../resultadosPruebas/grafico1.png";
          
          document.getElementById("demo").appendChild(img);
          var att = document.createAttribute("class");
          att.value = "img-responsive";
          img.setAttributeNode(att);
          }

          if (i==1) {
            var p = document.createElement("p");
          p.innerHTML = "El histograma representa la cantidad de ocurrencias de cada número. En el eje x se observa cada número perteneciente al rango [1;1000], el eje y muestra la cantidad de veces que apareció cada valor y por ultimo los distintos colores indican el puesto en que salio cada valor.";
          document.getElementById("demo").appendChild(p);

          var img = document.createElement("img");
          img.src = "../../resultadosPruebas/grafico2.png";
          
          document.getElementById("demo").appendChild(img);
          var att = document.createAttribute("class");
          att.value = "img-responsive";
          img.setAttributeNode(att);
          }

          if (i==2) {
            var p = document.createElement("p");
          p.innerHTML = "En el gráfico se muestra un diagrama de cajas donde se reconoce fácilmente la mediana en color verde, como así también permite visualizar, a través de los cuartiles, cómo es la distribución, su grado de asimetría y los valores extremos. En el eje x se observa la posición de cada valor, es decir, x=1 representa todos los valores que aparecieron en la primera posición de cada secuencia. El eje y representa cada número perteneciente al rango [1;1000].";
          document.getElementById("demo").appendChild(p);

          var img = document.createElement("img");
          img.src = "../../resultadosPruebas/grafico3.png";
          document.getElementById("demo").appendChild(img);
          var att = document.createAttribute("class");
          att.value = "img-responsive";
          img.setAttributeNode(att);

          }

          var hr = document.createElement("hr");
          document.getElementById("demo").appendChild(hr);

        }
      </script> 
    </div>
    <div class="col-sm-2 sidenav">
    </div>
  </div>
</div>


<?php include 'Estructura/footer.php';?>


</body>
</html>





