<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
.navbar {
      margin-bottom: 0;
      border-radius: 0;
}
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {height: 450px}
    
    /* Set gray background color and 100% height */
.sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
}
    
    /* Set black background color, white text and some padding */
html, body {
    margin: 0;
    height: 100%;
}

    
    /* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
  .sidenav {
  		min-height: 100%;
        height: 100%;
        padding: 0px;
  }
  .row.content {height:100%;} 
}
  </style>
	<title></title>
</head>
<body>
	<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand">Servicio Aleatorio - IJAN</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="index.php">Introducción</a></li>
        <li><a href="resultados.php">Resultados de pruebas</a></li>
      </ul>
    </div>
  </div>
</nav>
</body>
</html>