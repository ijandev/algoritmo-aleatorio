<!DOCTYPE html>
<html lang="en">
<head>
  <title>Resultados de Pruebas</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
.navbar {
      margin-bottom: 0;
      border-radius: 0;
}
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {height: 450px}
    
    /* Set gray background color and 100% height */
.sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
}
    
    /* Set black background color, white text and some padding */
html, body {
    margin: 0;
    height: 100%;
}
    
    /* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
  .sidenav {
        min-height: 100%;
        height: 100%;
        padding: 0px;
  }
  .row.content {height:100%;} 
}
  </style>
  <link rel="stylesheet" type="text/css" href="Styles/style.css" media=”screen” />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  


</head>
<body>

<?php include 'Estructura/header.php';?>

<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      
    </div>
    <div class="col-sm-8 text-left"> 
      <h1>El algoritmo</h1>
      <p>Tiene como propósito retornar una secuencia de números enteros pseudoaleatorios criptográficamente seguros. El algoritmo fue desarrollado por becarios de la Facultad de Informática (FaI) de la Universidad Nacional del Comahue, a partir de un convenio realizado con el Instituto Provincial de Juegos de Azar del Neuquén (IJAN)</p>
      <hr>

      <h1>El Repositorio</h1>
      <p>El proyecto se encuentra almacenado en un repositorio remoto de GitLab, al cual se puede acceder mediante la siguiente url de manera pública:</p>
      <a href="https://gitlab.com/ijandev/algoritmo-aleatorio.git">Repositorio de GitLab</a>
      <hr>

      <h1>Las Pruebas</h1>
      <p>A la hora de desarrollar un generador de secuencias pseudoaleatorio, se busca que cada número tenga la misma probabilidad de aparición en cada posición de la secuencia. Es decir, que la ocurrencia de cada número se distribuye uniformemente. Una distribución uniforme se caracteriza por tener un valor de media aritmética, que coincida con el promedio de los valores que componen la muestra. Además es importante analizar la dispersión de los datos reflejada en el desvío estándar. Así un desvío estándar alto, es el valor deseable, ya que indica que la dispersión de los datos obtenida abarca desde el valor mínimo al máximo de la muestra. Para evaluar el algoritmo se genera una simulación de una cantidad determinada de sorteos, todos con el mismo valor de "n", para que las pruebas sean significativas. La simulación de sorteos realizada se la llamara muestra de la población, a la cual se le analiza la media aritmética y el desvío estándar. Y así obtener el comportamiento estadístico del algoritmo desarrollado. La media aritmética se obtiene sumando todos los datos y el resultado es divido por el total de los elementos. Esta medida es importante conocerla dado que en las distribuciones uniformes, el promedio de los valores tiende a ser el valor medio. Por otra parte, el desvío estándar, al ser una medida de dispersión, si los datos se distribuyen uniformemente, tiende a acercarse a la raíz cuadrada del valor medio, para cubrir los valores extremos.</p>
      <hr>

      <h1>La Extracción</h1>
      <p>En esta primera instancia del flujo de datos del experimento, es necesario recolectar una muestra significativa de datos del entorno de producción. Para ello se utilizó en las pruebas del algoritmo el módulo  recuestter que simula, utilizando la librería Requests, la realización de una cantidad determinada de sorteos con el mismo valor de  n. Las peticiones al servicio para armar la muestra, se realizan  del  mismo modo que una petición real.

La ejecución de este módulo, puebla la Base de Datos del sistema con el historial de solicitudes y los resultados brindados. Sobre estos datos almacenados se realiza la extracción y el muestreo.

Luego de generados los datos, se extraen utilizando el módulo parser, que también participa del proceso de transformación, siendo el punto de bisagra entre los datos crudos de la DB y un conjunto de datos separados por coma, con un tamaño ideal para realizar el análisis correspondiente.</p>
      <hr>


      <h1>Transformación y Limpieza</h1>
      <p>Con el objetivo de sistematizar las pruebas sobre el set de datos obtenido, el módulo llamado parser, es el encargado de extraer los datos de la base y transformarlos al formato .csv para luego poder ser analizados.

Para ello hace uso de la librería Pandas que permite generar una estructura matricial de datos (llamada DataFrame) de forma optimizada a partir de una conexión con una DB y una sentencia SQL.
Una vez obtenido el DataFrame, la primer tarea de Limpieza fue transformar los datos almacenados como una cadena de caracteres separados por comas, en números enteros sin signo, uno por columna representando el orden o posición en la secuencia.

Esta transformación numérica debe ser con una precisión suficiente para la tarea, pero a su vez óptima en tamaño para resguardar la performance del flujo de trabajo. Durante todo el proceso este enfoque fue prioritario y se puede observar en cada uno de los componentes del sistema. Para llevar a cabo esta tarea de precisión numérica se utiliza la librería de calculo científico Numpy, que además de contener un amplio conjunto de herramientas de cálculo, la ejecución de su core es realizada internamente en lenguaje C conocido por su velocidad de ejecución.

Finalmente mediante el uso de Pandas, es posible almacenar los datos como valores separados por comas o .csv. De esta manera, se garantiza que las pruebas puedan ser replicadas tanto en el despliegue local del servidor, como cada vez que sea requerido en otros ambientes.
 
El script agg.py es el encargado de construir una matriz compuesta en cada celda con la sumatoria de la cantidad de ocurrencias  que salio un número en una determinada posición

Una vez que los datos ya están en el formato indicado, son cargados y evaluados por el componente graphic, encargado de generar los valores estadísticos relevantes y las visualizaciones que permiten interpretar fácilmente los resultados obtenidos. Para ello se hace uso nuevamente de la librería Pandas, tanto para la lectura de los archivos csv como para la descripción estadística de los mismos.</p>
      <hr>





      <div id="demo"></div>

    </div>
    <div class="col-sm-2 sidenav">
    </div>
  </div>
</div>
  
<?php include 'Estructura/footer.php';?>



</body>
</html>

