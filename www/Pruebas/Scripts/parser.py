import mysql.connector as mariadb
import numpy as np
import pandas as pd
import time

from pandas.core.frame import DataFrame


if __name__ == "__main__":

    # Conectar con la base
    conn = mariadb.connect(user='nombre_usuario',
        password="contraseña",
        host="ip",
        database="nombre_db")
    df = DataFrame()
    n=100
    ran = list(range(1,n+1))
    # chunksize cantidad de secuencias a procesar por bloque
    df_mysql = pd.read_sql('select secuencia_aleatoria  FROM respuesta  \
                INNER JOIN solicitud on solicitud.idsolicitud = respuesta.idsolicitud \
                     WHERE solicitud.n=100', con=conn,chunksize=100000)
    i=0
    for chunk in df_mysql:
        print(f'chunk{i}')
        df = chunk['secuencia_aleatoria'].str.split(
            ';', expand=True)
        df = df.astype(np.uint16)
        df.columns = ran
        df.to_csv(f'csv/{n}/Df-{n}-{i}.csv')
        i=i+1
  
    
    print(df.columns)
    conn.close()