<?php

include_once("util/funciones.php");
// Se recupera información de la petición
$datos = data_submitted();

try {
         $arrayDirectorios = array('../html');
         $objHash = new HashEntorno();
         $objHash->verificarHash($arrayDirectorios);
  	      $objSecAl = new SecAleatoria($datos);
  	      $objAut = new Autenticacion($datos);
	
         $datos['idusuario'] = $objAut->getIdUsuario();
         // Se almacena  la solicitud en la base de datos
         $objSolicitud =  new Solicitud($datos);
         $objSolicitud->insertar(); 	

         // Se genera la secuencia aleatoria
         $arrSec = $objSecAl ->generarSecuencia();

         // Se almacena  la respuesta en la base de datos
         $datos['sec_al'] = $arrSec;
         $objResp =  new Respuesta($datos);
         $objResp->setObjSolicitud($objSolicitud);
         $objResp->insertar(); 	
        
         // Se arma la salida
         $salida = [ "result" => ["RNG" =>$arrSec]	];
         $arrSec = json_encode($salida);

         


    
} catch (Exception $e) {
   if ($e->getMessage() == 'Falta parametro n.') {
      header("Location: CVista/VisualizadorImagenes/index.php", true, 301);
      exit();
   } else {
      // echo 'Excepción capturada: ',  $e->getMessage(), "\n";
      $salida = ["message" => $e->getMessage()];
      $arrSec = json_encode($salida);
   }
   

   
	
}	
	
   print_r($arrSec);
   return $arrSec;
   
?>
	
