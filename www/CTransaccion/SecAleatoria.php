<?php
class SecAleatoria {

	private $n;

	public function __construct($param){
	
			if( ! isset($param['n']) )
				throw new Exception('Falta parametro n.');
			if( ! is_numeric($param['n']))
				throw new Exception('El parametro n debe ser un numero.');
			if( ! ($param['n']>0) )
				throw new Exception('El parametro n debe ser >0.');
			if(! ($param['n']<=100000) )
				throw new Exception('El parametro n debe ser <100000.');
			$this->n = $param['n'];

	}
 	
 	public function setN($param){
		$this->n=$param;
	}
	
	public function getN(){
		return $this->n ;
	}


	public function generarSecuencia(){
		//$arr=array();
		$cantNumeros=$this->getN();
		$i=0;
		$arr = range(1, $cantNumeros);
		$length=sizeof($arr);
		
		while ($i < $length) {
			
			$val = random_int($i, $length-1);
	        $aux = $arr[$i];
			$arr[$i] = $arr[$val]; 
			$arr[$val] = $aux;
   			$i++;

   			
			
		}
	
		return $arr;
	}
}

?>
