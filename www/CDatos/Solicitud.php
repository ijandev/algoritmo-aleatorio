<?php

class Solicitud{

	private $n;
	private $idusuario;
	private $idsolicitud;
	private $fecha;
	private $tabla ;
	private $campos;
	private $mensajeoperacion;
//idsolicitud	n	fecha	idusuario		

	public function __construct($param){
		$this->tabla = "solicitud";
		$this->campos = "n,idusuario";
		$this->n = $param['n'];
		$this->idusuario = (isset($param['idusuario']))?$param['idusuario'] : null  ;
		$this->idsolicitud = (isset($param['idsolicitud']))?$param['idsolicitud'] : null  ;
		
	}

	public function getTabla(){
		return $this->tabla;
	}
	
	public function getCampos(){
		return $this->campos;
	}
	public function setIdsolicitud($param){
		$this->idsolicitud=$param;
	}

	public function getIdsolicitud(){
		return $this->idsolicitud;
	}

    public function setN($param){
		$this->n=$param;
	}
	public function getN(){
		return $this->n ;
	}

	public function setFecha($param){
		$this->fecha=$param;
	}

	public function getFecha(){
		return $this->fecha ;
	}

	public function getIdUsuario(){
		return $this->idusuario ;
	}	

	 public function setIdUsuario($param){
		$this->idusuario=$param;
	}

	public function setmensajeoperacion($param){

		$this->mensajeoperacion =$param;
	}
	
	public function getmensajeoperacion(){
		return $this->mensajeoperacion ;
	}
	
	
	

		//fecha	idusuario	idrespuesta


	/**
	 * Recupera los datos dada su clave
	 * @param $clave
	 * @return true en caso de encontrar los datos, false en caso contrario 
	 */		
    public function Buscar($clave){
		$base=new BaseDatos();
		$consultaPersona="Select * from ".$this->tabla ."  where idsolicitud=".$clave['idsolicitud'];
		$resp= false;
		if($base->Iniciar()){
			if($base->Ejecutar($consultaPersona)){
				if($row2=$base->Registro()){
					$this->setIdsolicitud($clave['idsolicitud']);					
				    $this->setIdUsuario($row2['idusuario']);
					$this->setFecha($row2['fecha']);
					$this->setN($row2['n']);
					$resp= true;
				}				
			
		 	}	else {
		 			$this->setmensajeoperacion($base->getError());
		 		
			}
		 }	else {
		 		$this->setmensajeoperacion($base->getError());
		 	
		 }		
		 return $resp;
	}	
    

	public static function listar($condicion=""){
	    $coleccion = null;
		$base=new BaseDatos();
		$consulta ="Select * from  ".$this->tabla;
		if ($condicion!=""){
		    $consulta=$consulta.' where '.$condicion;
		}
		//echo $consulta;
		if($base->Iniciar()){
			if($base->Ejecutar($consulta)){				
				$coleccion= array();
				while($row2=$base->Registro()){

					$obj = new Solicitud();
					$obj->cargar($row2);
					array_push($coleccion,$obj);
	
				}
				
			
		 	}	
		 }	
		 return $coleccion;
	}	


	
	public function insertar(){

		try {
			$base=new BaseDatos();
		$resp= false;
		$consultaInsertar="INSERT INTO ".$this->getTabla()."(".$this->getCampos().") 
				VALUES (".$this->getN().",".$this->getIdUsuario().")";
		//echo $consultaInsertar;
		if($base->Iniciar()){

			if($id=$base->devuelveIDInsercion($consultaInsertar)){
				$this->setIdsolicitud($id);
			    $resp=  true;

			}	else {
					$this->setmensajeoperacion($base->getError());
					
			}

		} else {
				$this->setmensajeoperacion($base->getError());
			
		}
		} catch (Exception $e) {
			throw new Exception($e);
			
		}

		
		return $resp;
	}
	

	public function modificar(){
	    $resp =false; 
	    $base=new BaseDatos();
		$consultaModifica="UPDATE ". $this->getTabla()." SET n='".$this->getN().
                           "' WHERE idsolicitud =". $this->getIdsolicitud();
		if($base->Iniciar()){
			if($base->Ejecutar($consultaModifica)){
			    $resp=  true;
			}else{
				$this->setmensajeoperacion($base->getError());
				
			}
		}else{
				$this->setmensajeoperacion($base->getError());
			
		}
		return $resp;
	}
	
	public function eliminar(){
		$base=new BaseDatos();
		$resp=false;
		if($base->Iniciar()){
				$consultaBorra="DELETE FROM $this->getTabla() WHERE idsolicitud=".$this->getIdsolicitud();
				if($base->Ejecutar($consultaBorra)){
				    $resp=  true;
				}else{
						$this->setmensajeoperacion($base->getError());
					
				}
		}else{
				$this->setmensajeoperacion($base->getError());
			
		}
		return $resp; 
	}

	
}
?>
